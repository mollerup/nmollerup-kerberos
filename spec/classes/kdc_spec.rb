require 'spec_helper'

describe 'kerberos::kdc::config', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    path = $test_config_dir[facts[:osfamily]]
    context "on #{os}" do
      let(:facts) do
        facts
      end
      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_file("#{path}/kdc.conf") }
    end
  end
end

describe 'kerberos::kdc', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('kerberos::kdc') }
      it { is_expected.to contain_class('kerberos::kdc::install') }
      it { is_expected.to contain_class('kerberos::kdc::config') }
      it { is_expected.to contain_class('kerberos::kdc::service') }
    end
  end
end
