require 'spec_helper'

describe 'kerberos::client::config', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end
      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_file('/etc/krb5.conf') }
    end
  end
end

describe 'kerberos::client', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('kerberos::client') }
      it { is_expected.to contain_class('kerberos::client::install') }
      it { is_expected.to contain_class('kerberos::client::config') }
      it { is_expected.to contain_class('kerberos::client::service') }
    end
  end
end
