require 'spec_helper'

describe 'kerberos::kprop::config', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    path = $test_config_dir[facts[:osfamily]]
    context "on #{os}" do
      let(:facts) do
        facts
      end
      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_file("#{path}/kpropd.acl") }
    end
  end
end

describe 'kerberos::kprop', type: 'class' do
  on_supported_os($test_os).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('kerberos::kprop') }
      it { is_expected.to contain_class('kerberos::kprop::install') }
      it { is_expected.to contain_class('kerberos::kprop::config') }
      it { is_expected.to contain_class('kerberos::kprop::service') }
    end
  end
end
